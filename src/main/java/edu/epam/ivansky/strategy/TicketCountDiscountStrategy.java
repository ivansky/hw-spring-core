package edu.epam.ivansky.strategy;

import edu.epam.ivansky.domain.Event;
import edu.epam.ivansky.domain.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;

@Component
public class TicketCountDiscountStrategy implements DiscountStrategy {

  @Value("${strategy.ticketCount.count}")
	private long ticketCount;

  @Value("${strategy.ticketCount.discount}")
	private long discount;

	public long getTicketCount() {
		return ticketCount;
	}

	public void setTicketCount(long ticketCount) {
		this.ticketCount = ticketCount;
	}

	public long getDiscount() {
		return discount;
	}

	public void setDiscount(long discount) {
		this.discount = discount;
	}

	@Override
	public byte getDiscount(@Nullable User user, @Nonnull Event event, @Nonnull LocalDateTime airDateTime, long numberOfTickets) {
		long ticketsCountBefore = 0;

		if(user != null) {
			ticketsCountBefore = user.getTickets().size();
		}

		long ticketsDiscounted = (long) Math.ceil((double) (ticketsCountBefore % numberOfTickets + numberOfTickets) / numberOfTickets);

		return (byte) ((ticketsDiscounted * discount) / numberOfTickets);
	}

}
