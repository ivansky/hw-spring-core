package edu.epam.ivansky.strategy;

import edu.epam.ivansky.domain.Event;
import edu.epam.ivansky.domain.Ticket;
import edu.epam.ivansky.domain.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Component
public class BirthdayDiscountStrategy implements DiscountStrategy {

  @Value("${strategy.birthday.discount}")
	private byte discount;

  @Value("${strategy.birthday.interval}")
  private byte interval;

	public void setDiscount(byte discount) {
		this.discount = discount;
	}

	public byte getInterval() {
		return interval;
	}

	public void setInterval(byte interval) {
		this.interval = interval;
	}

	public BirthdayDiscountStrategy() {
		setInterval((byte) 5);
		setDiscount((byte) 5);
	}

	public byte getDiscount(User user, Ticket ticket) {
		LocalDate birthday = user.getBirthday();
		LocalDateTime ticketDate = ticket.getDateTime();
		long daysLeft = ChronoUnit.DAYS.between(birthday, ticketDate);
		return 0;
	}

	@Override
	public byte getDiscount(@Nullable User user, @Nonnull Event event, @Nonnull LocalDateTime airDateTime, long numberOfTickets) {
		if(user == null || user.getBirthday() == null){
			return 0;
		}

		long daysLeft = ChronoUnit.DAYS.between(user.getBirthday(), airDateTime);

		return daysLeft <= interval?  discount : 0;
	}
}
