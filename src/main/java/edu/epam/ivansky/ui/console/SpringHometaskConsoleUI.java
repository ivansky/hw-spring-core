package edu.epam.ivansky.ui.console;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.epam.ivansky.config.AppConfig;
import org.springframework.context.ApplicationContext;

import edu.epam.ivansky.domain.Auditorium;
import edu.epam.ivansky.domain.Event;
import edu.epam.ivansky.domain.EventRating;
import edu.epam.ivansky.domain.Ticket;
import edu.epam.ivansky.domain.User;
import edu.epam.ivansky.service.AuditoriumService;
import edu.epam.ivansky.service.BookingService;
import edu.epam.ivansky.service.EventService;
import edu.epam.ivansky.service.UserService;
import edu.epam.ivansky.ui.console.state.MainState;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Simple console UI application for the hometask code. UI provides different
 * action to input and output data. In order for the application to work, the
 * Spring context initialization code should be placed into
 * {@link #initContext()} method.
 *
 * @author Yuriy_Tkach
 */
public class SpringHometaskConsoleUI {

	private ApplicationContext context;

	public static void main(String[] args) {
		SpringHometaskConsoleUI ui = new SpringHometaskConsoleUI();
		ui.initContext();
		ui.run();
	}

	private void initContext() {
		context = new AnnotationConfigApplicationContext(AppConfig.class);
	}

	private void run() {
		System.out.println("Welcome to movie theater console service");

		fillInitialData();

		MainState state = new MainState(context);

		state.run();

		System.out.println("Exiting.. Thank you.");
	}

	private void fillInitialData() {
		UserService userService = context.getBean(UserService.class);
		EventService eventService = context.getBean(EventService.class);
		AuditoriumService auditoriumService = context.getBean(AuditoriumService.class);
		BookingService bookingService = context.getBean(BookingService.class);

		//Auditorium auditorium = auditoriumService.getAll().iterator().next();

		Auditorium auditorium = new Auditorium();

		auditorium.setName("Some auditoria");
		auditorium.setNumberOfSeats(10);

		Set<Long> vipSeats = new HashSet<>();
		vipSeats.add(1L);
		vipSeats.add(2L);
		vipSeats.add(3L);
		auditorium.setVipSeats(vipSeats);

        auditoriumService.save(auditorium);

		if (auditorium == null) {
			throw new IllegalStateException("Failed to fill initial data - no auditoriums returned from AuditoriumService");
		}
		if (auditorium.getNumberOfSeats() <= 0) {
			throw new IllegalStateException("Failed to fill initial data - no seats in the auditorium " + auditorium.getName());
		}

		User user = new User();
		user.setEmail("my@email.com");
		user.setFirstName("Foo");
		user.setLastName("Bar");

		user = userService.save(user);

		Event event = new Event();
		event.setName("Grand concert");
		event.setRating(EventRating.MID);
		event.setBasePrice(10);
		LocalDateTime airDate = LocalDateTime.of(2020, 6, 15, 19, 30);
		event.addAirDateTime(airDate, auditorium);

		event = eventService.save(event);

		Ticket ticket1 = new Ticket(user, event, airDate, 1);
		bookingService.bookTickets(Collections.singleton(ticket1));

		if (auditorium.getNumberOfSeats() > 1) {
			User userNotRegistered = new User();
			userNotRegistered.setEmail("somebody@a.b");
			userNotRegistered.setFirstName("A");
			userNotRegistered.setLastName("Somebody");
			Ticket ticket2 = new Ticket(userNotRegistered, event, airDate, 2);
			bookingService.bookTickets(Collections.singleton(ticket2));
		}
	}
}
