package edu.epam.ivansky.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.time.LocalDateTime;

@Entity
@Table(
        name = "event_air_dates",
        uniqueConstraints = @UniqueConstraint(columnNames = {"eventId", "date"})
)
public class EventAirDate extends DomainObject {


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eventId")
    Event event;

    @Column
    LocalDateTime date;

}
