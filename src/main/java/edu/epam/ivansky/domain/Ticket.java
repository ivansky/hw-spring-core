package edu.epam.ivansky.domain;

import javax.annotation.Nullable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(
        name = "tickets",
        uniqueConstraints=
        @UniqueConstraint(columnNames={"eventId", "dateTime", "seat"})
)
public class Ticket extends DomainObject implements Comparable<Ticket>, Serializable {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
	private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eventId", nullable = false)
	private Event event;

    @Column(nullable = false)
	private LocalDateTime dateTime;

    @Column
	private long seat;

    @Column
	private boolean purchased;

	public Ticket(@Nullable User user, Event event, LocalDateTime dateTime, long seat) {
		this.user = user;
		this.event = event;
		this.dateTime = dateTime;
		this.seat = seat;
	}

	public User getUser() {
		return user;
	}

	public Event getEvent() {
		return event;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public long getSeat() {
		return seat;
	}

	public boolean isPurchased() {
		return purchased;
	}

	public void setPurchased(boolean purchased) {
		this.purchased = purchased;
	}

	@Override
	public int hashCode() {
		return Objects.hash(dateTime, event, seat);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Ticket other = (Ticket) obj;
		if (dateTime == null) {
			if (other.dateTime != null) {
				return false;
			}
		} else if (!dateTime.equals(other.dateTime)) {
			return false;
		}
		if (event == null) {
			if (other.event != null) {
				return false;
			}
		} else if (!event.equals(other.event)) {
			return false;
		}
		if (seat != other.seat) {
			return false;
		}
		return true;
	}

	@Override
	public int compareTo(Ticket other) {
		if (other == null) {
			return 1;
		}
		int result = dateTime.compareTo(other.getDateTime());

		if (result == 0) {
			result = event.getName().compareTo(other.getEvent().getName());
		}
		if (result == 0) {
			result = Long.compare(seat, other.getSeat());
		}
		return result;
	}

}
