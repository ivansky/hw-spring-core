package edu.epam.ivansky.service.impl;

import edu.epam.ivansky.domain.Event;
import edu.epam.ivansky.domain.User;
import edu.epam.ivansky.service.DiscountService;
import edu.epam.ivansky.strategy.DiscountStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class DiscountServiceImpl implements DiscountService {

	private List<DiscountStrategy> discountStrategies;

  @Autowired
	public DiscountServiceImpl(List<DiscountStrategy> discountStrategies) {
		this.discountStrategies = discountStrategies;
	}

	public List<DiscountStrategy> getDiscountStrategies() {
		return discountStrategies;
	}

	public void setDiscountStrategies(List<DiscountStrategy> discountStrategies) {
		this.discountStrategies = discountStrategies;
	}

	@Override
	public byte getDiscount(@Nullable User user, @Nonnull Event event, @Nonnull LocalDateTime airDateTime, long numberOfTickets) {
		return getDiscountStrategies()
				.stream()
				.map(discountStrategy -> discountStrategy.getDiscount(user, event, airDateTime, numberOfTickets))
				.max(Byte::compare)
				.get();
	}
}
