package edu.epam.ivansky.service.impl;

import edu.epam.ivansky.dao.AuditoriumDAO;
import edu.epam.ivansky.domain.Auditorium;
import edu.epam.ivansky.service.AuditoriumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

@Service
public class AuditoriumServiceImpl implements AuditoriumService {

    @Autowired
    private AuditoriumDAO auditoriumDAO;

    @Override
    public Auditorium save(@Nonnull Auditorium object) {
        return auditoriumDAO.save(object);
    }

    @Override
    public void remove(@Nonnull Auditorium object) {
        auditoriumDAO.remove(object);
    }

    @Nullable
    @Override
    public Auditorium getById(@Nonnull Long id) {
        return auditoriumDAO.getById(id);
    }

    @Nonnull
    @Override
    public Set<Auditorium> getAll() {
        return auditoriumDAO.getAll();
    }

    @Nullable
    @Override
    public Auditorium getByName(@Nonnull String name) {
        return auditoriumDAO.getByName(name);
    }
}
