package edu.epam.ivansky.service.impl;

import edu.epam.ivansky.dao.TicketDAO;
import edu.epam.ivansky.domain.*;
import edu.epam.ivansky.service.BookingService;
import edu.epam.ivansky.service.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BookingServiceImpl implements BookingService {

	@Value("${booking.markupHighEventRating}")
	private double markupForHighRatingEvent;

  @Value("${booking.markupVipRating}")
  private double markupForVipSeat;

	@Autowired
	private TicketDAO ticketDAO;

	@Autowired
	private DiscountService discountService;

	public TicketDAO getTicketDAO() {
		return ticketDAO;
	}

	public void setTicketDAO(TicketDAO ticketDAO) {
		this.ticketDAO = ticketDAO;
	}

	public double getMarkupForHighRatingEvent() {
		return markupForHighRatingEvent;
	}

	public void setMarkupForHighRatingEvent(double markupForHighRatingEvent) {
		this.markupForHighRatingEvent = markupForHighRatingEvent;
	}

	public double getMarkupForVipSeat() {
		return markupForVipSeat;
	}

	public void setMarkupForVipSeat(double markupForVipSeat) {
		this.markupForVipSeat = markupForVipSeat;
	}

	public DiscountService getDiscountService() {
		return discountService;
	}

	public void setDiscountService(DiscountService discountService) {
		this.discountService = discountService;
	}

	@Override
	public double getTicketsPrice(@Nonnull Event event, @Nonnull LocalDateTime dateTime, @Nullable User user, @Nonnull Set<Long> seats) {
		double totalPrice;

		long numberOfTickets = seats.size();
		byte discount = discountService.getDiscount(user, event, dateTime, numberOfTickets);

		double basePrice = event.getBasePrice();
		EventRating rating = event.getRating();
		double currentPrice = (rating.equals(EventRating.HIGH)) ? basePrice * markupForHighRatingEvent : basePrice;

		Auditorium auditorium = event.getAuditoriums().entrySet()
				.stream()
				.filter(entry -> entry.getKey().equals(dateTime))
				.map(Map.Entry::getValue)
				.findFirst()
				.get();

		Set<Long> vipSeats = auditorium.getVipSeats();

		totalPrice = seats
				.stream()
				.mapToDouble(seat -> vipSeats.contains(seat)? basePrice * markupForVipSeat : currentPrice)
				.sum();

		return totalPrice * ((double) discount / 100.0);
	}

	@Override
	public void bookTickets(@Nonnull Set<Ticket> tickets) {
		for (Ticket ticket : tickets) {
			ticket.setPurchased(true);
			User user = ticket.getUser();
			if (user != null && user.getId() != null) {
				user.getTickets().add(ticket);
			}
		}
	}

	@Nonnull
	@Override
	public Set<Ticket> getPurchasedTicketsForEvent(@Nonnull Event event, @Nonnull LocalDateTime dateTime) {
		return ticketDAO.getAll()
				.stream()
				.filter(ticket -> ticket.isPurchased() && ticket.getEvent().equals(event) && ticket.getDateTime().equals(dateTime))
				.collect(Collectors.toSet());
	}
}
