package edu.epam.ivansky.dao;

import edu.epam.ivansky.domain.Auditorium;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

public interface AuditoriumDAO extends AbstractDomainObjectDAO<Auditorium> {

    @Nullable
    Auditorium getByName(@Nonnull String name);

    @Nonnull
    Set<Auditorium> getAll();

}
