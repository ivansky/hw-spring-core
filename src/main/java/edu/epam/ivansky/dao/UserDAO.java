package edu.epam.ivansky.dao;

import edu.epam.ivansky.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface UserDAO extends AbstractDomainObjectDAO<User> {

  @Nullable
  User getUserByEmail(@Nonnull String email);

}
