package edu.epam.ivansky.dao;

import edu.epam.ivansky.domain.Event;
import edu.epam.ivansky.domain.LuckyEvent;
import edu.epam.ivansky.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public interface LuckyEventDAO extends AbstractDomainObjectDAO<LuckyEvent> {

    @Nonnull
    List<LuckyEvent> getByUser(@Nonnull User user);

    @Nonnull
    List<LuckyEvent> getByEvent(@Nonnull Event event);

    @Nullable
    LuckyEvent getByUserAndEvent(@Nonnull User user, @Nonnull Event event);

}
