package edu.epam.ivansky.dao.impl;

import edu.epam.ivansky.dao.LuckyEventDAO;
import edu.epam.ivansky.domain.Event;
import edu.epam.ivansky.domain.LuckyEvent;
import edu.epam.ivansky.domain.User;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

@Repository("luckyEventDao")
public class LuckyEventDAOImpl extends AbstractDAOImpl<LuckyEvent> implements LuckyEventDAO {

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public List<LuckyEvent> getByUser(@Nonnull User user) {
        return (List<LuckyEvent>) sessionFactory.getCurrentSession()
                .createCriteria(LuckyEvent.class)
                .add(Restrictions.eq("userId", user.getId())).list();
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public List<LuckyEvent> getByEvent(@Nonnull Event event) {
        return (List<LuckyEvent>) sessionFactory.getCurrentSession()
                .createCriteria(LuckyEvent.class)
                .add(Restrictions.eq("eventId", event.getId())).list();
    }

    @Nullable
    @Override
    public LuckyEvent getByUserAndEvent(@Nonnull User user, @Nonnull Event event) {
        return (LuckyEvent) sessionFactory.getCurrentSession()
                .createCriteria(LuckyEvent.class)
                .add(Restrictions.eq("userId", user.getId()))
                .add(Restrictions.eq("eventId", event.getId()))
                .uniqueResult();
    }

}
