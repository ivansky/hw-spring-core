package edu.epam.ivansky.dao.impl;

import edu.epam.ivansky.dao.UserDAO;
import edu.epam.ivansky.domain.User;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Repository("userDao")
public class UserDAOImpl extends AbstractDAOImpl<User> implements UserDAO {

    @Nullable
    @Override
    public User getUserByEmail(@Nonnull String email) {
        return (User) sessionFactory.getCurrentSession()
                .createCriteria(User.class)
                .add(Restrictions.eq("email", email))
                .uniqueResult();
    }

}
