package edu.epam.ivansky.dao.impl;

import edu.epam.ivansky.dao.AbstractDomainObjectDAO;
import edu.epam.ivansky.domain.DomainObject;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.List;

@Component
@Transactional(propagation = Propagation.REQUIRED)
abstract class AbstractDAOImpl<T extends DomainObject> extends StorableDAOImpl<T> implements AbstractDomainObjectDAO<T> {

    private Class<T> persistenceClass;

    /**
     * Accessing Generic Types At Runtime
     */
    @PostConstruct
    @SuppressWarnings("unchecked")
    protected void readGenericType() {
        persistenceClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }


    @Override
    @SuppressWarnings("unchecked")
    public T getById(@Nonnull Long id) {
        return (T) sessionFactory.getCurrentSession().get(persistenceClass, id);
    }

    @Override
    public T save(@Nonnull T object) {
        return put(object);
    }

    @Override
    public void remove(@Nonnull T object) {
        super.remove(object);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<T> getAll() {
        return (List<T>) sessionFactory.getCurrentSession().createCriteria(persistenceClass).list();
    }

}
