package edu.epam.ivansky.dao.impl;

import edu.epam.ivansky.dao.EventAirDateDAO;
import edu.epam.ivansky.domain.Event;
import edu.epam.ivansky.domain.EventAirDate;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Repository("eventAirDateDao")
public class EventAirDateDAOImpl extends AbstractDAOImpl<EventAirDate> implements EventAirDateDAO {

    @Nullable
    @Override
    public EventAirDate getByEventAndDate(@Nonnull Event event, @Nonnull LocalDateTime dateTime) {
        return (EventAirDate) sessionFactory.getCurrentSession()
                .createCriteria(EventAirDate.class)
                .add(Restrictions.eq("eventId", event.getId()))
                .add(Restrictions.eq("date", Timestamp.valueOf(dateTime)))
                .uniqueResult();
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public List<EventAirDate> getByEvent(@Nonnull Event event) {
        return (List<EventAirDate>) sessionFactory.getCurrentSession()
                .createCriteria(EventAirDate.class)
                .add(Restrictions.eq("eventId", event.getId()))
                .list();
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public List<EventAirDate> getByDate(@Nonnull LocalDateTime dateTime) {
        return (List<EventAirDate>) sessionFactory.getCurrentSession()
                .createCriteria(EventAirDate.class)
                .add(Restrictions.eq("eventId", Timestamp.valueOf(dateTime)))
                .list();
    }

}
