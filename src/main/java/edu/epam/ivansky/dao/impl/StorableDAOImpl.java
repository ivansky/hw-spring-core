package edu.epam.ivansky.dao.impl;

import edu.epam.ivansky.domain.DomainObject;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.io.Serializable;

@Component
abstract class StorableDAOImpl<T extends DomainObject> {

    @Autowired
    protected SessionFactory sessionFactory;

    @Transactional
    @Nonnull
    T put(T object) {
        Serializable id = sessionFactory.getCurrentSession().save(object);
        object.setId((Long) id);
        return object;
    }

    @Transactional
    void remove(T object) {
        sessionFactory.getCurrentSession().delete(object);
    }

}
