package edu.epam.ivansky.dao.impl;

import edu.epam.ivansky.dao.EventDAO;
import edu.epam.ivansky.domain.Event;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@Repository("eventDao")
public class EventDAOImpl extends AbstractDAOImpl<Event> implements EventDAO {

    @Nullable
    @Override
    public Event getByName(@Nonnull String name) {
        return (Event) sessionFactory.getCurrentSession()
                .createCriteria(Event.class)
                .add(Restrictions.eq("name", name))
                .uniqueResult();
    }

    @Override
    public Event save(@Nonnull Event event) {
        event = super.save(event);

        event.
    }
}
