package edu.epam.ivansky.dao.impl;

import edu.epam.ivansky.dao.AuditoriumDAO;
import edu.epam.ivansky.domain.Auditorium;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Repository("auditoriumDao")
public class AuditoriumDAOImpl extends AbstractDAOImpl<Auditorium> implements AuditoriumDAO {

    @Nullable
    @Override
    public Auditorium getByName(@Nonnull String name) {
        return (Auditorium) sessionFactory.getCurrentSession()
                .createCriteria(Auditorium.class)
                .add(Restrictions.eq("name", name))
                .uniqueResult();
    }

    @Transactional
    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public Set<Auditorium> getAll() {
        return ((List<Auditorium>) sessionFactory.getCurrentSession()
                .createCriteria(Auditorium.class).list())
                .stream().collect(Collectors.toSet());
    }

}
