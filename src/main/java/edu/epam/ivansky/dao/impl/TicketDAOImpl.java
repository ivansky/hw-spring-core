package edu.epam.ivansky.dao.impl;

import edu.epam.ivansky.dao.TicketDAO;
import edu.epam.ivansky.domain.Ticket;
import org.springframework.stereotype.Repository;

@Repository("TicketDao")
public class TicketDAOImpl extends AbstractDAOImpl<Ticket> implements TicketDAO {

}
