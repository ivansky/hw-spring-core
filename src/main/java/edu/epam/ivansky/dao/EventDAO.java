package edu.epam.ivansky.dao;

import edu.epam.ivansky.domain.Event;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface EventDAO extends AbstractDomainObjectDAO<Event> {

  @Nullable
  Event getByName(@Nonnull String name);

}
