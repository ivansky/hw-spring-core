package edu.epam.ivansky.dao;

import edu.epam.ivansky.domain.Ticket;

public interface TicketDAO extends AbstractDomainObjectDAO<Ticket> {

}
