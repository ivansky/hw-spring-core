package edu.epam.ivansky.dao;

import edu.epam.ivansky.domain.Event;
import edu.epam.ivansky.domain.EventAirDate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.List;

public interface EventAirDateDAO extends AbstractDomainObjectDAO<EventAirDate> {

    @Nullable
    EventAirDate getByEventAndDate(@Nonnull Event event, @Nonnull LocalDateTime dateTime);

    @Nonnull
    List<EventAirDate> getByEvent(@Nonnull Event event);

    @Nonnull
    List<EventAirDate> getByDate(@Nonnull LocalDateTime dateTime);

}
