package edu.epam.ivansky.aspect;

import edu.epam.ivansky.domain.Event;
import edu.epam.ivansky.domain.Ticket;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Aspect
public class CounterAspect {

    private static final Map<Event, Integer> counterForNameRequest = new HashMap<>();
    private static final Map<Event, Integer> counterForPriceRequest = new HashMap<>();
    private static final Map<Event, Integer> bookingCounter = new HashMap<>();

    @AfterReturning(
            pointcut = "execution(* edu.epam.ivansky.service.impl.EventServiceImpl.getByName(..))",
            returning = "event"
    )
    public void countCallGetEventByName(Event event) {
        if (event != null) {
            Integer counter = counterForNameRequest.getOrDefault(event, 0);
            counterForNameRequest.put(event, counter + 1);

            System.out.println("[ASPECT] Counter for event name request: " + counterForNameRequest);
        }
    }

    @AfterReturning("execution(* edu.epam.ivansky.service.BookingService.getTicketsPrice(..))")
    public void countCallGetEventPrice(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        Event event = (Event) args[0];
        if (event != null) {
            Integer counter = counterForPriceRequest.getOrDefault(event, 0);
            counterForPriceRequest.put(event, counter + 1);

            System.out.println("[ASPECT] Counter for price request: " + counterForPriceRequest);
        }
    }

    @AfterReturning("execution(* edu.epam.ivansky.service.impl.BookingServiceImpl.bookTickets(..))")
    public void countCallBookTickets(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        Set<Ticket> tickets = (Set<Ticket>) args[0];
        tickets.forEach(ticket -> {
            Event event = ticket.getEvent();
            if (event != null) {
                Integer counter = bookingCounter.getOrDefault(event, 0);
                bookingCounter.put(event, counter + 1);

                System.out.println("[ASPECT] Booking counter: " + bookingCounter);
            }
        });
    }


}
