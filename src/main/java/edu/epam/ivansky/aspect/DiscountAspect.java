package edu.epam.ivansky.aspect;

import edu.epam.ivansky.domain.Event;
import edu.epam.ivansky.domain.User;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Aspect
@Component
public class DiscountAspect {

  @AfterReturning(
      pointcut = "cflow(call(* *.bookTickets(..))) " +
          "&& execution(* edu.epam.ivansky.strategy.DiscountStrategy.getDiscount(..))",
      returning = "result")
  public void countDiscount(JoinPoint joinPoint, Object result) {
    Object[] arguments = joinPoint.getArgs();

    User user = (User) arguments[0];
    Event event = (Event) arguments[1];
    LocalDateTime airDateTime = (LocalDateTime) arguments[2];
    long numberOfTickets = (Long) arguments[3];

    System.out.println(joinPoint.getStaticPart());

    for(Object arg : arguments) {
      System.out.println(arg);
    }

  }

}
