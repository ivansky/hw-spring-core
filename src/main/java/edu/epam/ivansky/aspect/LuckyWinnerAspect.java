package edu.epam.ivansky.aspect;

import edu.epam.ivansky.dao.LuckyEventDAO;
import edu.epam.ivansky.domain.Event;
import edu.epam.ivansky.domain.LuckyEvent;
import edu.epam.ivansky.domain.User;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

@Aspect
@Component
public class LuckyWinnerAspect {

    @Autowired
    private LuckyEventDAO luckyEventDAO;

    private static final int LUCKY_NUMBER = 42;

    @Around("execution(double edu.epam.ivansky.service.impl.BookingServiceImpl.getTicketsPrice(..))")
    public double callGetTicketsPrice(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        Event event = (Event) args[0];
        User user = (User) args[2];

        LuckyEvent luckyEvent = null;

        if(user != null) {
            luckyEvent = luckyEventDAO.getByUserAndEvent(user, event);
        }

        if (isLucky()) {
            if (user != null) {

                if(luckyEvent == null) {
                    luckyEvent = new LuckyEvent(user, event);
                }

                System.out.println("[ASPECT] Lucky user is " + user);
            }

            return 0.0;
        }

        return (double) joinPoint.proceed(args);
    }

    private boolean isLucky() {
        return (new Random().nextInt() % 100) == LUCKY_NUMBER;
    }


}
